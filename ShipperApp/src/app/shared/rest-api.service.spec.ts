import { TestBed, inject} from '@angular/core/testing';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';

import { RestApiService } from './rest-api.service';

describe('RestApiService', () => {
  let service: RestApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RestApiService]
     });
    service = TestBed.inject(RestApiService);
  });

  it(
    'should get shippers',
    inject(
      [HttpTestingController,RestApiService],
      (httpMock: HttpTestingController, service:RestApiService) => {
        const mockShippers = [
          {id: 0, companyName: 'First Test Shipper', phone: '(0123) 23424455'},
          {id: 1, companyName: 'Second Test Shipper', phone: '+44 234234 1324132'}
        ];
        service.getShippers().subscribe((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Response:
              expect(event.body).toEqual(mockShippers);
          }
        })
      }
    )
  )

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
