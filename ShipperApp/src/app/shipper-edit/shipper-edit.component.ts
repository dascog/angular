import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';
import { Shipper } from '../shared/shipper';

@Component({
  selector: 'app-shipper-edit',
  templateUrl: './shipper-edit.component.html',
  styleUrls: ['./shipper-edit.component.css']
})
export class ShipperEditComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  shipperDetails!:Shipper;
  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.restApi.getShipper(this.id).subscribe((data: Shipper) => {
      this.shipperDetails = data;
    })
  }

  updateShipper() {
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateShipper(this.id, this.shipperDetails).subscribe(data => {
        this.router.navigate(['/shipper-list'])
      })
    }
  }

}
