## UI for the LolChampions REST API
In this project you will go through the steps we followed yesterday to create a UI for the LolChampions REST API we developed in the Java course. You will need to be able to access all the CRUD operations.

1. Create a new Angular project (using ``ng new``) with a suitable name for your UI

* remember that you will want to enable routing, and use CSS as your style sheet option.
* if you want to use bootstrap styles, then ``npm install bootstrap`` in the project home directory, and add  ``"node_modules/bootstrap/dist/css/bootstrap.min.css",`` to the appropriate place in your angular.json file.

2. Create the components you need using the ``ng g c command``

* You will need a component for listing all Lol Champions, for creating a new entry, and for editing an existing entry.

3. Put in place the routing you need to switch between your components - make sure the list component is the default

* First step is to edit the routes list in the app-routing.module.ts
* Next delete everything but the router-outlet from your app.componet.html template, and add your first couple of routing links. You can use buttons or links.

4. Add HttpClientModule to the imports at the top of your app.module.ts file, and make sure it appears in the @NgModule imports array of the same file.

5. Create an entity class to hold your LolChampions data

* It is a good idea to put this in a shared directory ``ng g class shared/LolChampion``
* The class should include all the attributes of a LOL Champion and initialize with defaults

6. Create a rest api service using ``ng g service``

* This should be in your shared directory too.
* You will be linking to either your own Lol Champions API, or you can use the group one at
http://franks-lolchampion-demo-franks-lolchampion-demo.emeadocker65.conygre.com/swagger-ui/
* use the swagger-ui interface to determine the endpoints you need for your service.
* You can use the rest-api.service.ts you created for the shippers UI as a model, but you will need to check that the endpoints match!

7. Add FormsModule to the app.module.ts and make sure it is in the imports array

8. Edit the list component so the rest-api service is injected into the constructor, and then create functions corresponding to the endpoints of the REST api 

* These will list all the Lol Champions, load a champion by ID, delete by ID, edit by ID, etc.

9. Edit the list component template to display your loaded list of champions using the Shippers template to guide you.

10. Edit the create and edit components to complete functionality of the app.

11. If you have time, Frank's Lol Champions REST api also includes the URL of an image for each Champion. See if you can display this in your list of Lol C
