**Angular Training Materials**

- Compiled for Citi Grads EMEA 2021.
- Designed to introduce the Angular Framework and demonstrate creating a UI that will consume a REST api.
- Requires the REST api to be running on the localhost:8080/api/shippers. This is the Spring Boot example students should have already created by this point in the course. 

## Contents

1. AngularSlides.zip:		This is the slideset used for the course. In reality only about 40% of these slides will be used, the rest are there for reference.
2. AngularDemos.zip: 		Angular demo projects that accompany the slides.
3. Angular12FullCourse.zip:	A more complete Angular course for reference.
4. AngularCodeSnippets.txt:	This accompanies the presentation and gives step-by-step code to introduce into your angular project and complete the final UI.
5. AngularCodeSteps.docx:	This is a text guide for the instructor to cover the presentation/code 
6. ShipperApp:			Another completed version of the ShippersApp, but this also includes Frank's Dockerfile and Jenkinsfile for deployment.
7. README.md:			This file
